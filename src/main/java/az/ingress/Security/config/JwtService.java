package az.ingress.Security.config;
import az.ingress.Security.model.UserDb;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.stream.Collectors;


@Component
public class JwtService {

    private Key key;
    @Value("${spring.secret.key}")
    private String secret;
    private byte[] bytes;


    @PostConstruct
    public void init() {
        bytes = Decoders.BASE64.decode(secret);//decodes
        key = Keys.hmacShaKeyFor(bytes);//encodes
    }


    public Claims parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String generateToken(UserDb userDb){
        return Jwts.builder()
                .setSubject(userDb.getName())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(Duration.ofHours(1))))
                .signWith(key, SignatureAlgorithm.HS256)
                .claim("authority",userDb.getAuthorityList().stream().map(authority -> authority.getAuthorities().name()).collect(Collectors.toList()))
                .compact();
    }
}


