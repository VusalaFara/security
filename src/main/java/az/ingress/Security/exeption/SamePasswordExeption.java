package az.ingress.Security.exeption;

public class SamePasswordExeption extends RuntimeException{

    public SamePasswordExeption(String message){
        super(message);
    }
}
