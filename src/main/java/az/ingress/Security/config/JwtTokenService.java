package az.ingress.Security.config;

import az.ingress.Security.service.impl.UserDetailsServiceImplement;
import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
@Component
public class JwtTokenService {

    private final JwtService jwtParser;
    private final UserDetailsServiceImplement userDetailsService;

    public Optional<Authentication>getAthentication(HttpServletRequest request){
        log.info("passed from here");
        return Optional.ofNullable(request.getHeader("Authorization"))
                .filter(header->isBearerToken(header))
                .map(this::getAuthentication);//in this case 'this' means data inside stream
    }

    private Authentication getAuthentication(String header) {
       String token= header.substring("bearer ".length()).trim();
        Claims claims = jwtParser.parseToken(token);
        log.info("claimsss   "+claims);
        if(!claims.getExpiration().before(new Date())){
            return getAuthentication(claims);
        }throw new RuntimeException("something went wrong");
    }

    private boolean isBearerToken(String header) {
        return header.toLowerCase().startsWith("bearer ");
    }
//    private Authentication getAuthentication(Claims claims){
//        log.info("claimsss");
//        UserDetails userDetails = userDetailsService.loadUserByUsername(claims.get("name",String.class));
//        log.info("subjectttt"+ claims.getSubject());
//        List<GrantedAuthority>authorities =
//        List.of(new SimpleGrantedAuthority(claims.get("authority",String.class)));
//        return new UsernamePasswordAuthenticationToken(userDetails,null,authorities);
//    }

    private Authentication getAuthentication(Claims claims) {
        log.info("claimsss");
//        UserDetails userDetails = userDetailsService.loadUserByUsername(claims.get("name", String.class));
        UserDetails userDetails = userDetailsService.loadUserByUsername(claims.getSubject());
        log.info("subjectttt   " + claims.getSubject());

        Object authorityClaim = claims.get("authority");

        if (authorityClaim instanceof String) {
            // If the claim is already a String, use it as is
            List<GrantedAuthority> authorities = List.of(new SimpleGrantedAuthority((String) authorityClaim));
            return new UsernamePasswordAuthenticationToken(userDetails, null, authorities);
        } else if (authorityClaim instanceof ArrayList) {
            // If the claim is an ArrayList, convert it to a String
            List<GrantedAuthority> authorities = ((ArrayList<?>) authorityClaim).stream()
                    .map(Object::toString)
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());
            return new UsernamePasswordAuthenticationToken(userDetails, null, authorities);
        } else {
            // Handle other cases or throw an exception as needed
            throw new IllegalArgumentException("Invalid authority claim type");
        }
    }
}
