package az.ingress.Security.advice;


import az.ingress.Security.exeption.InvalidAuthorityExeption;
import az.ingress.Security.exeption.SamePasswordExeption;
import az.ingress.Security.exeption.UserExsistsExeption;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;


@RestControllerAdvice
public class SecurityExeptionHandler {

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(InvalidAuthorityExeption.class)
    public Map<String,String> handleInvalidAuthorityExeption(InvalidAuthorityExeption ex) {
        Map<String, String> errorMap = new HashMap<>();
        errorMap.put("error message: ", ex.getMessage());
        return errorMap;
    }
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public Map<String,String> handleNotReadableExeption(HttpMessageNotReadableException ex) {
        Map<String, String> errorMap = new HashMap<>();
        String message = "Not Valid Authority!     "+ex.getMessage();
        errorMap.put("error message: ", message);
        return errorMap;
    }
    @ExceptionHandler(UserExsistsExeption.class)
    public ResponseEntity<Object>handleUserExsistsExeption(UserExsistsExeption ex){
        return new ResponseEntity<>(ex.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(SamePasswordExeption.class)
    public ResponseEntity<Object>handleSamePasswordExeption(SamePasswordExeption ex){
        return new ResponseEntity<>(ex.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
