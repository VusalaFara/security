package az.ingress.Security.model;

public enum Authorities {

    PUBLIC,
    USER,
    ADMIN
}
