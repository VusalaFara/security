package az.ingress.Security.dto;

import az.ingress.Security.model.Authority;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegisterRequestDto {

    private String username;
    private String password;
    private String repeatPassword;
    List<Authority> authorityList;
}
