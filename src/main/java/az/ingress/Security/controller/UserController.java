package az.ingress.Security.controller;

import az.ingress.Security.dto.JwtResponseDto;
import az.ingress.Security.dto.LoginDto;
import az.ingress.Security.dto.RegisterRequestDto;
import az.ingress.Security.exeption.InvalidAuthorityExeption;
import az.ingress.Security.service.UserdbService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/security")
@RequiredArgsConstructor
public class UserController {

    private final UserdbService userdbService;


    @GetMapping("/user")
    public String getUser(){
        return "User";
    }

    @DeleteMapping ("/delete")
    public String deleteAdmin(){
        return "deleteadmin";
    }

    @PutMapping ("/put")
    public String putAdmin(){
        return "put";
    }

    @GetMapping("/public")
    public String getPublic(){
        return "public";
    }

    @PostMapping("/public")
    public String postPublic(){
        return "public";
    }

    @GetMapping("/admin")
    public String getAdmin(){
        return "admin";
    }

    @PostMapping("/register")
    public ResponseEntity<JwtResponseDto>register(@RequestBody RegisterRequestDto requestDto) throws InvalidAuthorityExeption {
        return new ResponseEntity<JwtResponseDto>(userdbService.register(requestDto), HttpStatus.OK);
    }
    @PostMapping("/login")
    public String login(@RequestBody LoginDto loginDto,@RequestHeader("Authorization") String token){
        return userdbService.login(loginDto,token);
    }
}
