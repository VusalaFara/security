package az.ingress.Security.service.impl;

import az.ingress.Security.model.MyUserPrincipal;
import az.ingress.Security.model.UserDb;
import az.ingress.Security.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;


import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserDetailsServiceImplement implements UserDetailsService {

    private final UserRepository userRepository;


    @Override
//    @SneakyThrows
//    @Transactional
    public UserDetails loadUserByUsername(String username){
     UserDb user = userRepository.findByName(username).orElseThrow(RuntimeException::new);
        log.info("userrrrr "+user);
        //            String usernameValue = user.get().getName();
        //            Set<SimpleGrantedAuthority> authorities = user.get().getAuthorityList().stream()
        //                    .map(authority -> new SimpleGrantedAuthority(authority.getAuthorities().name()))
        //                    .collect(Collectors.toSet());
        //            log.info("authorities "+ authorities);
        //            log.info("usernameValue "+ usernameValue);
        //
        //            if (StringUtils.hasText(usernameValue) && !authorities.isEmpty()) {
        //                return User.builder()
        //                        .username(usernameValue)
        //                        .authorities(authorities)
        //                        .build();
        //            }
//            Set<SimpleGrantedAuthority> authoritySet = user.getAuthorityList().stream().map(authority ->
//                    new SimpleGrantedAuthority(authority.getAuthorities().name())).collect(Collectors.toSet());
//            return User.builder()
//                    .username(user.getName())
//                    .authorities(authoritySet)
//                    .build();
        return new MyUserPrincipal(user);
    }
}


