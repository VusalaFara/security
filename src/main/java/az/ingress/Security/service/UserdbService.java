package az.ingress.Security.service;

import az.ingress.Security.dto.JwtResponseDto;
import az.ingress.Security.dto.LoginDto;
import az.ingress.Security.dto.RegisterRequestDto;
import az.ingress.Security.exeption.InvalidAuthorityExeption;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;

@Service
public interface UserdbService {
    JwtResponseDto register(RegisterRequestDto requestDto) throws InvalidAuthorityExeption;

    String login(LoginDto loginDto,String token);
}
