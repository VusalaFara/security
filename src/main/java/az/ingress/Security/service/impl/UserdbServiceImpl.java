package az.ingress.Security.service.impl;

import az.ingress.Security.config.JwtService;
import az.ingress.Security.config.JwtTokenService;
import az.ingress.Security.dto.JwtResponseDto;
import az.ingress.Security.dto.LoginDto;
import az.ingress.Security.dto.RegisterRequestDto;
import az.ingress.Security.exeption.InvalidAuthorityExeption;
import az.ingress.Security.exeption.SamePasswordExeption;
import az.ingress.Security.exeption.UserExsistsExeption;
import az.ingress.Security.model.Authorities;
import az.ingress.Security.model.Authority;
import az.ingress.Security.model.UserDb;
import az.ingress.Security.repository.AuthorityRepository;
import az.ingress.Security.repository.UserRepository;
import az.ingress.Security.service.UserdbService;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatusCode;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserdbServiceImpl implements UserdbService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;


    @Override
    public JwtResponseDto register(RegisterRequestDto requestDto) throws InvalidAuthorityExeption {
        userRepository.findByName(requestDto.getUsername()).ifPresent(userDb -> {
            throw  new UserExsistsExeption("User already exsits");
        });
        if (!requestDto.getPassword().equals(requestDto.getRepeatPassword())){
            throw new SamePasswordExeption("Passwords are different!");
        }

//        List<Authority> authList = requestDto.getAuthorityList();
//
//        List<String> invalidAuthorities = authList.stream()
//                .filter(authority -> !Arrays.stream(Authorities.values())
//                        .anyMatch(authorityEnum -> authorityEnum.name().equalsIgnoreCase(authority.getAuthorities().name()))
//                )
//                .map(authority -> authority.getAuthorities().name())
//                .collect(Collectors.toList());
//
//        if (!invalidAuthorities.isEmpty()) {
//            throw new IllegalArgumentException("Invalid authorities: " +  invalidAuthorities);
//        }
        List<Authority> authList = requestDto.getAuthorityList();
        Boolean isvalid = authList.stream().anyMatch(authority -> isValidAuthority(authority.getAuthorities().name()));
        log.info("invalid authority type");
//        if (!isvalid)
//            throw new InvalidAuthorityExeption("Invalid authorities: ");

        UserDb userDb = UserDb.builder()
                .name(requestDto.getUsername())
                .password(passwordEncoder.encode(requestDto.getPassword()))
                .authorityList(requestDto.getAuthorityList())
                .build();
        UserDb saveduser= userRepository.save(userDb);
        String token = jwtService.generateToken(saveduser);
        return JwtResponseDto.builder()
                .jwt(token)
                .build();
    }
    public static boolean isValidAuthority(String authorityName) {
        Authorities authorities =  Authorities.valueOf(authorityName);
        if (authorities!=null){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public String login(LoginDto loginDto,String token) {
        userRepository.findByName(loginDto.getUsername()).ifPresent(userDb -> {
            System.out.println(userDb.getPassword());
        });
        String tokentrimmed = token.substring("bearer ".length()).trim();
        Claims claims = jwtService.parseToken(tokentrimmed);
        List<String>authorities=claims.get("authority",List.class);
        return tokentrimmed;
    }

}
