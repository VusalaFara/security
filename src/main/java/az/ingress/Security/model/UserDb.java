package az.ingress.Security.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDb {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;

    String password;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "userdb_authority",
    joinColumns = @JoinColumn(name = "userdb_id"),
    inverseJoinColumns = @JoinColumn(name = "authority_id"))
    List<Authority>authorityList = new ArrayList<>();
}
