package az.ingress.Security.repository;

import az.ingress.Security.model.UserDb;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface UserRepository extends JpaRepository<UserDb,Long> {

    Optional<UserDb> findByName(String username);


}
